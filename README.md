# Sockethook

Heavily based on https://github.com/aws-samples/simple-websockets-chat-app

Here is an overview of the files:

```text
.
├── deployHooks/                       <-- Directory for storing deployment hooks
├── .gitignore                         <-- Gitignore for Stackery
├── .stackery-config.yaml              <-- Default CLI parameters for root directory
├── README.md                          <-- This README file
└── template.yaml                      <-- SAM infrastructure-as-code template
```

