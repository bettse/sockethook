exports.handler = async (event, context) => {
  // Log the event argument for debugging and for use in local development.
  console.log(JSON.stringify(event, undefined, 2));

  try {
    const { body } = event;

    return {
      statusCode: 200,
      body,
    };
  } catch (e) {
    console.log(e);
  }
};
