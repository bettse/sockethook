# Sockethook

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[![Netlify Status](https://api.netlify.com/api/v1/badges/fee2530d-65fe-4270-a7c0-94c8c4aba4ab/deploy-status)](https://app.netlify.com/sites/sockethook/deploys)

## Production

Visit https://sockethook.ericbetts.dev/ for details and instructions

## Development

In the project directory, you can run:

### `netlify dev`

Runs the app in the development mode.<br />
Open [http://localhost:9000](http://localhost:9000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

