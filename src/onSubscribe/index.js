const AWS = require('aws-sdk');
const crypto = require('crypto');

const ddb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  region: process.env.AWS_REGION,
});
const {TABLE_NAME} = process.env;

exports.handler = async (event, context) => {
  // Log the event argument for debugging and for use in local development.
  console.log(JSON.stringify(event, undefined, 2));
  const {body, requestContext} = event;
  const {connectionId} = requestContext;
  const message = JSON.parse(body);
  let {key, path} = message;
  if (!key && !path) {
    return {statusCode: 500, body: 'Missing key or path'};
  }

  if (path && !key) {
    const hash = crypto.createHash('sha256');
    hash.update(path);
    key = hash.digest('hex');
  }

  const putParams = {
    TableName: TABLE_NAME,
    Item: {
      connectionId,
      key,
    },
  };

  try {
    await ddb.put(putParams).promise();
  } catch (err) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        state: 'error',
        error: err,
      }),
    };
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      state: 'subscribed',
      key: key,
    }),
  };
};
