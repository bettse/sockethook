// Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

const crypto = require('crypto');
const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient({
  apiVersion: '2012-08-10',
  region: process.env.AWS_REGION,
});

const {API_CONNECTIONS_ENDPOINT, TABLE_NAME} = process.env;

exports.handler = async event => {
  // Log the event argument for debugging and for use in local development.
  console.log(JSON.stringify(event, undefined, 2));
  let connectionData;

  const {body = '', pathParameters} = event;
  const {key: path} = pathParameters;
  const hash = crypto.createHash('sha256');
  hash.update(path);
  const key = hash.digest('hex');

  try {
    connectionData = await ddb
      .query({
        TableName: TABLE_NAME,
        IndexName: 'byKey',
        KeyConditionExpression: '#key = :key',
        ExpressionAttributeValues: {
          ':key': key,
        },
        ExpressionAttributeNames: {
          '#key': 'key',
        },
        ProjectionExpression: 'connectionId',
      })
      .promise();
  } catch (e) {
    return {statusCode: 500, body: e.stack};
  }

  const apigwManagementApi = new AWS.ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint: API_CONNECTIONS_ENDPOINT,
  });

  const postCalls = connectionData.Items.map(async ({connectionId}) => {
    try {
      await apigwManagementApi
        .postToConnection({ConnectionId: connectionId, Data: body})
        .promise();
    } catch (e) {
      if (e.statusCode === 410) {
        console.log(`Found stale connection, deleting ${connectionId}`);
        await ddb
          .delete({TableName: TABLE_NAME, Key: {connectionId}})
          .promise();
      } else {
        throw e;
      }
    }
  });

  try {
    await Promise.all(postCalls);
  } catch (e) {
    return {statusCode: 500, body: e.stack};
  }

  return {
    statusCode: 200,
    body: `Data sent to ${postCalls.length} connections.`,
  };
};
